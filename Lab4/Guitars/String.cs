﻿namespace Lab4.Guitars
{
    public class String
    {
        public int Number { get; set; }
        public StringType Type { get; set; }

        public override string ToString()
        {
            return $"{Number}, {Type}";
        }
    }
}
