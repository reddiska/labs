﻿using System.Collections.Generic;
using System.Linq;

namespace Lab4.Guitars
{
    public class Guitar
    {
        public Body Body { get; set; }
        public Fretboard Fretboard { get; set; }
        public Neck Neck { get; set; }
        public List<String> Strings { get; set; }

        public Guitar()
        {
            Body = new Body();
            Neck = new Neck();
            Fretboard = new Fretboard();
            Strings = new List<String>();
        }

        public Guitar(List<String> strings)
        {
            Body = new Body();
            Neck = new Neck();
            Fretboard = new Fretboard();
            Strings = strings;
        }

        public string Play()
        {
            return Strings.First().Type == StringType.Nylon ? "Classic sound" : "Acoustic sound";
        }

        public string Configure()
        {
            return "Configured";
        }

        public void ChangeString(String newString)
        {
            if (newString == null )
                return;

            Strings.First(x => x.Number == newString.Number).Type = newString.Type;
        }
    }
}