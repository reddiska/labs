﻿using System;
using System.Collections.Generic;
using Lab4.Guitars;
using String = Lab4.Guitars.String;

namespace Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            var classicGuitar = CreateClassicGuitar();

            Console.WriteLine("Classic guitar:");
            Console.WriteLine(classicGuitar.Play());
            Console.WriteLine("...");

            var acousticGuitar = CreateAcousticGuitar();

            Console.WriteLine("Acoustic guitar:");
            Console.WriteLine(acousticGuitar.Play());
            Console.WriteLine("...");

            Console.WriteLine("Let's change strings in acoustic guitar!");
            Console.WriteLine("...");

            acousticGuitar.ChangeString(new String {Number = 1, Type = StringType.Nylon});
            acousticGuitar.ChangeString(new String {Number = 2, Type = StringType.Nylon});
            acousticGuitar.ChangeString(new String {Number = 3, Type = StringType.Nylon});
            
            Console.WriteLine("Acoustic guitar:");
            Console.WriteLine(acousticGuitar.Play());
            Console.WriteLine("...");
            Console.WriteLine("Oops! Acoustic guitar change its sound to classic!");

            Console.ReadKey();
        }

        private static Guitar CreateClassicGuitar()
        {
            var strings = new List<String>
            {
                new String
                {
                    Number = 1,
                    Type = StringType.Nylon
                },
                new String
                {
                    Number = 2,
                    Type = StringType.Nylon
                },
                new String
                {
                    Number = 3,
                    Type = StringType.Nylon
                },
                new String
                {
                    Number = 4,
                    Type = StringType.Metal
                },
                new String
                {
                    Number = 5,
                    Type = StringType.Metal
                },
                new String
                {
                    Number = 6,
                    Type = StringType.Metal
                }
            };
            return new Guitar(strings);
        }

        private static Guitar CreateAcousticGuitar()
        {
            var strings = new List<String>
            {
                new String
                {
                    Number = 1,
                    Type = StringType.Metal
                },
                new String
                {
                    Number = 2,
                    Type = StringType.Metal
                },
                new String
                {
                    Number = 3,
                    Type = StringType.Metal
                },
                new String
                {
                    Number = 4,
                    Type = StringType.Metal
                },
                new String
                {
                    Number = 5,
                    Type = StringType.Metal
                },
                new String
                {
                    Number = 6,
                    Type = StringType.Metal
                }
            };
            return new Guitar(strings);
        }
    }
}
