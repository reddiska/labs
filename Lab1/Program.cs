﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            var destination = "Minsk";
            var day = DayOfWeek.Monday;
            var time = TimeSpan.FromHours(5);
            
            var airlinesByDestination = GetAirlinesByDestination(GenerateAirlines(), destination);
            var airlinesByDay = GetAirlinesByDay(GenerateAirlines(), day);
            var airlinesByDayAndTime = GetAirlinesByDayAndTime(GenerateAirlines(), day, time);

            Console.WriteLine("airlinesByDestination");
            foreach (var airline in airlinesByDestination)
                Console.WriteLine(airline.ToString());
            Console.WriteLine();

            Console.WriteLine("airlinesByDay");
            foreach (var airline in airlinesByDay)
                Console.WriteLine(airline.ToString());
            Console.WriteLine();

            Console.WriteLine("airlinesByDayAndTime");
            foreach (var airline in airlinesByDayAndTime)
                Console.WriteLine(airline.ToString());

            Console.ReadKey();
        }

        private static IEnumerable<Airline> GenerateAirlines()
        {
            return new List<Airline>
            {
                new Airline
                {
                    Number = 1,
                    Destination = "Minsk",
                    PlaneType = PlaneType.Usual,
                    StartTime = TimeSpan.FromHours(9),
                    DaysOfWeeks = new List<DayOfWeek>
                    {
                        DayOfWeek.Monday,
                        DayOfWeek.Friday
                    }
                },
                new Airline
                {
                    Number = 2,
                    Destination = "Moscow",
                    PlaneType = PlaneType.Comfortable,
                    StartTime = TimeSpan.FromHours(11),
                    DaysOfWeeks = new List<DayOfWeek>
                    {
                        DayOfWeek.Monday,
                        DayOfWeek.Thursday,
                        DayOfWeek.Saturday
                    }
                }
            };
        }

        private static IEnumerable<Airline> GetAirlinesByDestination(IEnumerable<Airline> airlines, string destination)
        {
            return destination == null ? airlines : airlines.Where(x => string.Equals(x.Destination, destination));
        }

        private static IEnumerable<Airline> GetAirlinesByDay(IEnumerable<Airline> airlines, DayOfWeek day)
        {
            return day == default(DayOfWeek) ? airlines : airlines.Where(x => x.DaysOfWeeks.Contains(day));
        }

        private static IEnumerable<Airline> GetAirlinesByDayAndTime(IEnumerable<Airline> airlines, DayOfWeek day, TimeSpan time)
        {
            return day == default(DayOfWeek) || time == default(TimeSpan) ? airlines : airlines.Where(x => x.DaysOfWeeks.Contains(day) && x.StartTime > time);
        }
    }
}
