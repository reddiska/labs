﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab1
{
    public class Airline
    {
        public int Number { get; set; }
        public string Destination { get; set; }
        public PlaneType PlaneType { get; set; }
        public TimeSpan StartTime { get; set; }
        public List<DayOfWeek> DaysOfWeeks { get; set; }

        public Airline()
        {
            PlaneType = PlaneType.Usual;
            DaysOfWeeks = new List<DayOfWeek>();
        }

        public Airline(int number, string destination, PlaneType planeType, TimeSpan startTime,
            IEnumerable<DayOfWeek> daysOfWeeks)
        {
            Number = number;
            Destination = destination;
            PlaneType = planeType;
            StartTime = startTime;
            DaysOfWeeks = daysOfWeeks.ToList();
        }

        public override string ToString()
        {
            return $"{Number}: {Destination}";
        }
    }
}
