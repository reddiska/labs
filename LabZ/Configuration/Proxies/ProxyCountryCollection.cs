﻿using System.Configuration;

namespace LabZ.Configuration.Proxies
{
    public class ProxyCountryCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ProxyCountryElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ProxyCountryElement)element).Code;
        }
    }
}