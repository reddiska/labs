﻿using System.Configuration;

namespace LabZ.Configuration.Proxies
{
    public class ProxyCountryElement : ConfigurationElement
    {
        [ConfigurationProperty("code", IsRequired = true)]
        public string Code => (string)this["code"];

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name => (string)this["name"];
    }
}