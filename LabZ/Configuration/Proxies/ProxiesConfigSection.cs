﻿using System.Configuration;

namespace LabZ.Configuration.Proxies
{
    public class ProxiesConfigSection : ConfigurationSection
    {
        public override bool IsReadOnly()
        {
            return false;
        }

        [ConfigurationProperty("portionSize")] public int PortionSize => (int)this["portionSize"];

        [ConfigurationProperty("targetAmount")]
        public int TargetAmount => (int)this["targetAmount"];

        [ConfigurationProperty("checkLimit")] public int CheckLimit => (int)this["checkLimit"];

        [ConfigurationProperty("minSuccessRate")]
        public double MinSuccessRate => (double)this["minSuccessRate"];

        [ConfigurationProperty("downloadOnStartup")]
        public bool DownloadOnStartup => (bool)this["downloadOnStartup"];

        [ConfigurationProperty("proxyCheckFrequencyInHours")]
        public double ProxyCheckFrequencyInHours => (double)this["proxyCheckFrequencyInHours"];

        [ConfigurationProperty("downloadFrequencyInHours")]
        public double DownloadFrequencyInHours => (double)this["downloadFrequencyInHours"];

        [ConfigurationProperty("proxyCountries")]
        [ConfigurationCollection(typeof(ProxyCountryCollection), AddItemName = "country")]
        public ProxyCountryCollection ProxyCountries => (ProxyCountryCollection)this["proxyCountries"];
    }
}