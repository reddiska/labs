﻿using System.Configuration;

namespace LabZ.Configuration.TourOperators
{
    public class TourOperatorElement : ConfigurationElement
    {
        [ConfigurationProperty("id", IsRequired = true)]
        public int Id => (int)this["id"];

        [ConfigurationProperty("name")]
        public string Name => (string)this["name"];

        [ConfigurationProperty("getOnly", DefaultValue = false)]
        public bool GetOnly => (bool)this["getOnly"];

        [ConfigurationProperty("excluded", DefaultValue = false)]
        public bool Excluded => (bool)this["excluded"];
    }
}