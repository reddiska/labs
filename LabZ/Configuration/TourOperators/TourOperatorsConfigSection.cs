﻿using System.Configuration;
using LabZ.Configuration.Proxies;

namespace LabZ.Configuration.TourOperators
{
    public class TourOperatorsConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("operators")]
        [ConfigurationCollection(typeof(TourOperatorCollection), AddItemName = "operator")]
        public TourOperatorCollection TourOperators => (TourOperatorCollection)this["operators"];
    }
}