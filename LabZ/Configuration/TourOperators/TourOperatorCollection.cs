﻿using System.Configuration;

namespace LabZ.Configuration.TourOperators
{
    public class TourOperatorCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new TourOperatorElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((TourOperatorElement)element).Id;
        }
    }
}