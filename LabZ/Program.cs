﻿using System;
using System.Configuration;
using System.Linq;
using LabZ.Configuration.Proxies;
using LabZ.Configuration.TourOperators;

namespace LabZ
{
    internal class Program
    {
        private static void Main()
        {
            var proxyCountries = ((ProxiesConfigSection)ConfigurationManager
                    .GetSection("proxies"))
                .ProxyCountries
                .Cast<ProxyCountryElement>()
                .ToList();

            foreach (var proxyCountry in proxyCountries)
                Console.WriteLine($"{proxyCountry.Code}, {proxyCountry.Name}");

            var tourOperators = ((TourOperatorsConfigSection)ConfigurationManager
                    .GetSection("tourOperators"))
                .TourOperators
                .Cast<TourOperatorElement>()
                .ToList();

            foreach (var tourOperator in tourOperators)
                Console.WriteLine($"{tourOperator.Id}, {tourOperator.Name}, get only: {tourOperator.GetOnly}, excluded: {tourOperator.Excluded}");

            Console.ReadKey();
        }
    }
}