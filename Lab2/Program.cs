﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = 10.2;
            
            Console.WriteLine(GetSumOfPolynomials(GeneratePolynomials(), x));
            Console.ReadKey();
        }

        private static IEnumerable<Polynomial> GeneratePolynomials()
        {
            return new List<Polynomial>
            {
                new Polynomial
                {
                    A = new []{ 1.3, 4.2 },
                    C = 5.4,
                    N = 2
                },
                new Polynomial
                {
                    A = new []{ 1.2, 3.2, 0.4 },
                    C = 1.4,
                    N = 3
                },
                new Polynomial
                {
                    A = new []{ -1.8, 8.2, 0.1, -0.8 },
                    C = 5.2,
                    N = 4
                }
            };
        }

        private static double GetSumOfPolynomials(IEnumerable<Polynomial> polynomials, double x)
        {
            return polynomials != null ? polynomials.Sum(polynomial => polynomial.Calculate(x)) : 0;
        }
    }
}
