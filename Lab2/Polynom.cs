﻿using System;

namespace Lab2
{
    public class Polynomial
    {
        public double[] A { get; set; }
        public double C { get; set; }
        public int N { get; set; }

        public double Calculate(double x)
        {
            var sum = C;

            for (var i = 0; i < N; i++)
                sum += A[i] * Math.Pow(x, ++i);

            return sum;
        }
    }
}
