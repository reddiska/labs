﻿namespace Lab3
{
    public class Complex
    {
        public Rational Real { get; set; }
        public Rational Imaginary { get; set; }
    }
}