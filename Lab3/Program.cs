﻿using System;
using System.Collections.Generic;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.ReadKey();
        }

        private static IEnumerable<Complex> GenerateComplexes()
        {
            return new List<Complex>
            {
                new Complex
                {
                    Real = new Rational { },
                    Imaginary = new Rational { }
                },
                new Complex
                {
                    Real = new Rational { },
                    Imaginary = new Rational { }
                },
                new Complex
                {
                    Real = new Rational { },
                    Imaginary = new Rational { }
                }
            };
        }
    }
}
