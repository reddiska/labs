﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lab5.Jewelry;

namespace Lab5
{
    class Program
    {
        static void Main(string[] args)
        {
            var minTransparency = 0.05;
            var maxTransparency = 0.3;

            var necklace = new Necklace
            {
                Stones = new List<Stone>
                {
                    new SemipreciousStone
                    {
                        Price = 2,
                        Transparency = 0.5,
                        Weight = 13
                    },
                    new PreciousStone
                    {
                        Price = 10,
                        Transparency = 0.1,
                        Weight = 5
                    },
                    new PreciousStone
                    {
                        Price = 12,
                        Transparency = 0.2,
                        Weight = 7
                    },
                    new PreciousStone
                    {
                        Price = 14,
                        Transparency = 0.15,
                        Weight = 5
                    },
                    new SemipreciousStone
                    {
                        Price = 1,
                        Transparency = 0.4,
                        Weight = 11
                    }
                }
            };

            Console.WriteLine($"Price: {necklace.GetPrice()}");
            Console.WriteLine($"Weight: {necklace.GetWeight()}");

            Console.WriteLine();

            Console.WriteLine("Messed:");
            necklace.Stones.ForEach(Console.WriteLine);

            Console.WriteLine();
            
            Console.WriteLine("Sorted:");
            necklace.SortStones();
            necklace.Stones.ForEach(Console.WriteLine);

            Console.WriteLine();
            Console.WriteLine("With defined transparency:");
            
            necklace.FindStonesWithTransparency(minTransparency, maxTransparency)
                .ForEach(Console.WriteLine);

            Console.ReadKey();
        }
    }
}
