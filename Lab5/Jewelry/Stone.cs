﻿namespace Lab5.Jewelry
{
    public abstract class Stone
    {
        protected Stone()
        {
            Weight = 99;
            Price = 99999;
            Transparency = 0.9;
        }

        protected Stone(double weight, double price, double transparency)
        {
            Weight = weight;
            Price = price;
            Transparency = transparency;
        }

        public double Weight { get; set; }
        public double Price { get; set; }
        public double Transparency { get; set; }

        public override string ToString()
        {
            return $"Stone: Weight = {Weight}, price = {Price}, transparency = {Transparency}";
        }
    }
}