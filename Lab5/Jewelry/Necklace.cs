﻿using System.Collections.Generic;
using System.Linq;

namespace Lab5.Jewelry
{
    public class Necklace
    {
        public List<Stone> Stones { get; set; }

        public double GetWeight()
        {
            return Stones?.Sum(x => x.Weight) ?? 0;
        }

        public double GetPrice()
        {
            return Stones?.Sum(x => x.Price) ?? 0;
        }

        public void SortStones()
        {
            var sortedStones = new List<Stone>();

            sortedStones.AddRange(Stones.Where(x => x.GetType() == typeof(PreciousStone)));
            sortedStones.AddRange(Stones.Where(x => x.GetType() == typeof(SemipreciousStone)));

            Stones = sortedStones;
        }

        public List<Stone> FindStonesWithTransparency(double minTransparency, double maxTransparency)
        {
            return Stones
                .Where(x => x.Transparency >= minTransparency && x.Transparency <= maxTransparency)
                .ToList();
        }
    }
}
