﻿namespace Lab5.Jewelry
{
    public class SemipreciousStone : Stone
    {
        public SemipreciousStone() { }

        public SemipreciousStone(double weight, double price, double transparency)
            : base(weight, price, transparency) { }

        public override string ToString()
        {
            return $"Semiprecious: Weight = {Weight}, price = {Price}, transparency = {Transparency}";
        }
    }
}
