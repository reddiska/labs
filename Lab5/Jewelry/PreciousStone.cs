﻿namespace Lab5.Jewelry
{
    public class PreciousStone : Stone
    {
        public PreciousStone() : base() { } 

        public PreciousStone(double weight, double price, double transparency)
            : base(weight, price, transparency) { }

        public override string ToString()
        {
            return $"Precious: Weight = {Weight}, price = {Price}, transparency = {Transparency}";
        }
    }
}
