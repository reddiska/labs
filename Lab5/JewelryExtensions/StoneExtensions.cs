﻿using System.Collections.Generic;
using Lab5.Jewelry;

namespace Lab5.JewelryExtensions
{
    public static class StoneExtensions
    {
        public static KeyValuePair<double, double> GetWeightAndPrice(this Stone stone)
        {
            return new KeyValuePair<double, double>(stone.Weight, stone.Price);
        }
    }
}
