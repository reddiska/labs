﻿using Lab5.Jewelry;

namespace Lab5.JewelryFiltering
{
    public enum Condition
    {
        GreaterThen,
        LessThen,
        Equals
    }

    public interface IStoneCriteria
    {
        bool MeetCriteria(Stone stone);
    }
}