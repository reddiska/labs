﻿using System;
using Lab5.Jewelry;

namespace Lab5.JewelryFiltering
{
    public class OrCriteria : IStoneCriteria
    {
        public IStoneCriteria FirstCriteria { get; set; }
        public IStoneCriteria SecondCriteria { get; set; }

        public bool MeetCriteria(Stone stone)
        {
            try
            {
                return FirstCriteria.MeetCriteria(stone) | SecondCriteria.MeetCriteria(stone);
            }
            catch (NullReferenceException exception)
            {
                Console.WriteLine($"{exception.GetType()}: {exception.Message}");
                return true;
            }
        }
    }
}