﻿using System;
using Lab5.Jewelry;

namespace Lab5.JewelryFiltering
{
    public class WeightCriteria : IStoneCriteria
    {
        public double Value { get; set; }
        public Condition Condition { get; set; }

        public bool MeetCriteria(Stone stone)
        {
            const double tolerance = 0.001;
            switch (Condition)
            {
                case Condition.Equals:
                    return Math.Abs(stone.Weight - Value) < tolerance;
                case Condition.GreaterThen:
                    return stone.Weight > Value;
                case Condition.LessThen:
                    return stone.Weight < Value;
                default:
                    return false;
            }
        }
    }
}