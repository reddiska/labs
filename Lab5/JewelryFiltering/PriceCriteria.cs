﻿using Lab5.Jewelry;

namespace Lab5.JewelryFiltering
{
    public class PriceCriteria : IStoneCriteria
    {
        public int Value { get; set; }
        public Condition Condition { get; set; }

        public bool MeetCriteria(Stone stone)
        {
            switch (Condition)
            {
                case Condition.Equals:
                    return stone.Price == Value;
                case Condition.GreaterThen:
                    return stone.Price > Value;
                case Condition.LessThen:
                    return stone.Price < Value;
                default:
                    return false;
            }
        }
    }
}
