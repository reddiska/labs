import React, { Component } from 'react';
import uuid from 'uuid/v1';

import AppHeader from '../app-header';
import TodoList from '../todo-list';
import SearchPanel from '../search-panel';
import ItemStatusFilter from '../item-status-filter';
import ItemAddForm from '../item-add-form';

import './app.css';
import ApiService from '../../services/api-service';


export default class App extends Component {

  apiService = new ApiService()

  state = {
    items: [],
    filter: 'all',
    search: ''
  };

  componentDidMount = async () => {
    this.updateItems();
  }

  updateItems = async () => {
    this.setState({ items: await this.apiService.getAllToDos() })
  }

  onItemAdded = async (label) => {
    const item = this.createItem(label);

    await this.apiService.createToDo(item);
    await this.updateItems();
  };

  toggleProperty = (arr, id, propName) => {
    const idx = arr.findIndex((item) => item.id === id);
    const oldItem = arr[idx];
    oldItem[propName] = !oldItem[propName];

    return oldItem;
  };

  onToggleDone = async (id) => {
    await this.apiService.updateToDo(this.toggleProperty(this.state.items, id, 'done'));
    await this.updateItems();
  };

  onToggleImportant = async (id) => {
    await this.apiService.updateToDo(this.toggleProperty(this.state.items, id, 'important'));
    await this.updateItems();
  };

  onDelete = async (id) => {
    await this.apiService.deleteToDo(id);
    await this.updateItems();
  };

  onFilterChange = (filter) => {
    this.setState({ filter });
  };

  onSearchChange = (search) => {
    this.setState({ search });
  };

  createItem(label) {
    return {
      id: uuid(),
      label,
      important: false,
      done: false
    };
  }

  filterItems(items, filter) {
    if (filter === 'all') {
      return items;
    } else if (filter === 'active') {
      return items.filter((item) => (!item.done));
    } else if (filter === 'done') {
      return items.filter((item) => item.done);
    }
  }

  searchItems(items, search) {
    if (search.length === 0) {
      return items;
    }

    return items.filter((item) => {
      return item.label.toLowerCase().indexOf(search.toLowerCase()) > -1;
    });
  }

  render() {
    const { items, filter, search } = this.state;
    const doneCount = items.filter((item) => item.done).length;
    const toDoCount = items.length - doneCount;
    const visibleItems = this.searchItems(this.filterItems(items, filter), search);

    return (
      <div className="todo-app">
        <AppHeader toDo={toDoCount} done={doneCount}/>

        <div className="search-panel d-flex">
          <SearchPanel
            onSearchChange={this.onSearchChange}/>

          <ItemStatusFilter
            filter={filter}
            onFilterChange={this.onFilterChange} />
        </div>

        <TodoList
          items={ visibleItems }
          onToggleImportant={this.onToggleImportant}
          onToggleDone={this.onToggleDone}
          onDelete={this.onDelete} />

        <ItemAddForm
          onItemAdded={this.onItemAdded} />
      </div>
    );
  };
}
