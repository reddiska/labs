export default class ApiService {
  
  API_ROOT_URL = 'http://localhost:3001/api/todos/';

  getAllToDos = async () => {
    const response = await fetch(this.API_ROOT_URL);

    if (response.ok) 
      return await response.json();
    throw new Error(`Could not fetch, received ${response.status}`)
  }

  createToDo = async (todo) => {
    let response = await fetch(this.API_ROOT_URL, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(todo)
    });

    if (response.ok) 
      return await response.json();
    throw new Error(`Could not fetch, received ${response.status}`)
  }

  updateToDo = async (todo) => {
    let response = await fetch(this.API_ROOT_URL, {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(todo)
    });

    if (response.ok) 
      return await response.json();
    throw new Error(`Could not fetch, received ${response.status}`)
  }

  deleteToDo = async (id) => {
    let response = await fetch(this.API_ROOT_URL, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(id)
    });

    if (response.ok) 
      return;
    throw new Error(`Could not fetch, received ${response.status}`)
  }
}