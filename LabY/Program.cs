﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;

namespace LabY
{
    internal class Program
    {
        private const string UserAgent = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.162 Safari/535.19";
        //private const string OperatorUrl = "http://www.icstrvl.ru";
        private const string OperatorUrl = "https://oldsearch.intourist.ru/searchtournew.aspx";
        
        private static void Main()
        {
            var proxies = new List<string>
            {
                "95.84.154.73:57423",
                "95.46.158.251:80",
                "54.93.215.155:8081",
                "95.46.158.251:80",
                "35.158.101.150:3128",
                "51.77.147.98:3128",
                "54.93.215.155:8081",
            };

            foreach (var proxy in proxies)
            {
                var request = CreateRequest(proxy.Trim());
                Console.WriteLine($"Checking proxy {proxy}.");
                Console.Write("GET: ");
                Console.WriteLine(DoRequest(request));
                Console.WriteLine();
            }

            Console.ReadKey();
        }

        private static WebRequest CreateRequest(string proxy)
        {
            var request = WebRequest.CreateHttp(OperatorUrl);
            request.Proxy = new WebProxy(proxy, true);
            request.Method = WebRequestMethods.Http.Get;
            request.CookieContainer = new CookieContainer();
            request.UserAgent = UserAgent;
            return request;
        }

        private static string DoRequest(WebRequest request)
        {
            try
            {
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.MethodNotAllowed)
                        return "Not Allowed!";
                    var stopwatch = Stopwatch.StartNew();

                    using (var responseStream = response.GetResponseStream())
                    {
                        var statusCode = response.StatusCode.ToString();

                        if (responseStream == null)
                            return $"{statusCode}: No response!";

                        using (var streamReader = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            var responseText = streamReader.ReadToEnd();
                            if (string.IsNullOrEmpty(responseText))
                                return $"{statusCode}: Empty response!";
                        }

                        stopwatch.Stop();
                        return $"{statusCode}: Elapsed {stopwatch.Elapsed.TotalSeconds.ToString(CultureInfo.InvariantCulture)}";
                    }
                }
            }
            catch (Exception exception)
            {
                return $"ERROR: {exception.Message}";
            }
            
        }
    }
}
