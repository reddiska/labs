﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LabX
{
    internal class Program
    {
        private static void Main()
        {
            var uris = new List<Uri>
            {
                new Uri("http://www.mondepartement04.fr/accueil.html"),
                new Uri("http://www.resourceindex.com/"),
                new Uri("http://www.leroyanderson.com/"),
                new Uri("http://lhamo.tripod.com/"),
                new Uri("http://tef.karabuk.edu.tr/index.aspx"),
                new Uri("http://dcbu.org/"),
                new Uri("http://www.cnrs.fr/cw/dossiers/dosevol/accueil.html"),
                new Uri("https://sspw.pl/"),
                new Uri("https://www.adobe.com/products/coldfusion-family.html"),
                new Uri("https://www.alfaromeo.be/landing"),
                new Uri("http://www.metrovancouver.org/"),
                new Uri("http://www.mondepartement04.fr/accueil.html"),
                new Uri("http://www.resourceindex.com/"),
                new Uri("http://www.leroyanderson.com/"),
                new Uri("http://lhamo.tripod.com/"),
                new Uri("http://tef.karabuk.edu.tr/index.aspx"),
                new Uri("http://dcbu.org/"),
                new Uri("http://www.cnrs.fr/cw/dossiers/dosevol/accueil.html"),
                new Uri("https://sspw.pl/"),
                new Uri("https://www.adobe.com/products/coldfusion-family.html"),
                new Uri("https://www.alfaromeo.be/landing"),
                new Uri("http://www.metrovancouver.org/"),
                new Uri("http://www.mondepartement04.fr/accueil.html"),
                new Uri("http://www.resourceindex.com/"),
                new Uri("http://www.leroyanderson.com/"),
                new Uri("http://lhamo.tripod.com/"),
                new Uri("http://tef.karabuk.edu.tr/index.aspx"),
                new Uri("http://dcbu.org/"),
                new Uri("http://www.cnrs.fr/cw/dossiers/dosevol/accueil.html"),
                new Uri("https://sspw.pl/"),
                new Uri("https://www.adobe.com/products/coldfusion-family.html"),
                new Uri("https://www.alfaromeo.be/landing"),
                new Uri("http://www.metrovancouver.org/"),
                new Uri("http://www.mondepartement04.fr/accueil.html"),
                new Uri("http://www.resourceindex.com/"),
                new Uri("http://www.leroyanderson.com/"),
                new Uri("http://lhamo.tripod.com/"),
                new Uri("http://tef.karabuk.edu.tr/index.aspx"),
                new Uri("http://dcbu.org/"),
                new Uri("http://www.cnrs.fr/cw/dossiers/dosevol/accueil.html"),
                new Uri("https://sspw.pl/"),
                new Uri("https://www.adobe.com/products/coldfusion-family.html"),
                new Uri("https://www.alfaromeo.be/landing"),
                new Uri("http://www.metrovancouver.org/"),
                new Uri("http://www.mondepartement04.fr/accueil.html"),
                new Uri("http://www.resourceindex.com/"),
                new Uri("http://www.leroyanderson.com/"),
                new Uri("http://lhamo.tripod.com/"),
                new Uri("http://tef.karabuk.edu.tr/index.aspx"),
                new Uri("http://dcbu.org/"),
                new Uri("http://www.cnrs.fr/cw/dossiers/dosevol/accueil.html"),
                new Uri("https://sspw.pl/"),
                new Uri("https://www.adobe.com/products/coldfusion-family.html"),
                new Uri("https://www.alfaromeo.be/landing"),
                new Uri("http://www.metrovancouver.org/"),
                new Uri("http://www.mondepartement04.fr/accueil.html"),
                new Uri("http://www.resourceindex.com/"),
                new Uri("http://www.leroyanderson.com/"),
                new Uri("http://lhamo.tripod.com/"),
                new Uri("http://tef.karabuk.edu.tr/index.aspx"),
                new Uri("http://dcbu.org/"),
                new Uri("http://www.cnrs.fr/cw/dossiers/dosevol/accueil.html"),
                new Uri("https://sspw.pl/"),
                new Uri("https://www.adobe.com/products/coldfusion-family.html"),
                new Uri("https://www.alfaromeo.be/landing"),
                new Uri("http://www.metrovancouver.org/"),
                new Uri("http://www.mondepartement04.fr/accueil.html"),
                new Uri("http://www.resourceindex.com/"),
                new Uri("http://www.leroyanderson.com/"),
                new Uri("http://lhamo.tripod.com/"),
                new Uri("http://tef.karabuk.edu.tr/index.aspx"),
                new Uri("http://dcbu.org/"),
                new Uri("http://www.cnrs.fr/cw/dossiers/dosevol/accueil.html"),
                new Uri("https://sspw.pl/"),
                new Uri("https://www.adobe.com/products/coldfusion-family.html"),
                new Uri("https://www.alfaromeo.be/landing"),
                new Uri("http://www.metrovancouver.org/"),
                new Uri("http://www.mondepartement04.fr/accueil.html"),
                new Uri("http://www.resourceindex.com/"),
                new Uri("http://www.leroyanderson.com/"),
                new Uri("http://lhamo.tripod.com/"),
                new Uri("http://tef.karabuk.edu.tr/index.aspx"),
                new Uri("http://dcbu.org/"),
                new Uri("http://www.cnrs.fr/cw/dossiers/dosevol/accueil.html"),
                new Uri("https://sspw.pl/"),
                new Uri("https://www.adobe.com/products/coldfusion-family.html"),
                new Uri("https://www.alfaromeo.be/landing"),
                new Uri("http://www.metrovancouver.org/"),
                new Uri("http://www.mondepartement04.fr/accueil.html"),
                new Uri("http://www.resourceindex.com/"),
                new Uri("http://www.leroyanderson.com/"),
                new Uri("http://lhamo.tripod.com/"),
                new Uri("http://tef.karabuk.edu.tr/index.aspx"),
                new Uri("http://dcbu.org/"),
                new Uri("http://www.cnrs.fr/cw/dossiers/dosevol/accueil.html"),
                new Uri("https://sspw.pl/"),
                new Uri("https://www.adobe.com/products/coldfusion-family.html"),
                new Uri("https://www.alfaromeo.be/landing"),
                new Uri("http://www.metrovancouver.org/")
            };
            var sw = new Stopwatch();

            sw.Start();
            WithParallelForEach(uris, 6);
            sw.Stop();

            Console.WriteLine(sw.Elapsed.TotalSeconds);
            Console.ReadKey();
        }

        private static void WithSimpleForEach(IEnumerable<Uri> uris)
        {
            var webClient = new WebClient();
            foreach (var uri in uris)
            {
                webClient.DownloadData(uri);
                Console.WriteLine(uri);
            }
        }

        private static void WithParallelForEach(IEnumerable<Uri> uris, int threads)
        {
            Parallel.ForEach(
                uris,
                new ParallelOptions { MaxDegreeOfParallelism = threads },
                () => new WebClient(),
                (uri, loopState, index, webClient) =>
                {
                    webClient.DownloadData(uri);
                    Console.WriteLine("{0}:{1}", Thread.CurrentThread.ManagedThreadId, uri);
                    return webClient;
                },
                webClient => { });
        }

        private static void WithParallelLinq(IEnumerable<Uri> uris, int threads)
        {
            var webClient = new ThreadLocal<WebClient>(() => new WebClient());

            uris
                .AsParallel()
                .WithDegreeOfParallelism(threads)
                .ForAll(
                    uri =>
                    {
                        webClient.Value.DownloadData(uri);
                        Console.WriteLine("{0}:{1}", Thread.CurrentThread.ManagedThreadId, uri);
                    });
        }
    }
}