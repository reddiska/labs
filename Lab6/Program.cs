﻿using System;
using System.Linq;

namespace Lab6
{
    internal class Program
    {
        private static void Main()
        {
            var typeByClass = typeof(Stone);
            var typeByObject = new Stone().GetType();

            PlayWithConstructors(typeByClass);

            Console.ReadKey();
        }

        private static void PlayWithConstructors(Type type)
        {
            var constructors = type.GetConstructors();

            for (var i = 0; i < constructors.Length; i++)
            {
                var constructor = constructors[i];

                Console.WriteLine($"Constructor #{i}");
                Console.WriteLine($"IsPrivate: {constructor.IsPrivate}.");
                Console.WriteLine($"IsPublic: {constructor.IsPublic}.");
                Console.WriteLine($"IsStatic: {constructor.IsStatic}.");
                Console.WriteLine();

                var parameters = constructor.GetParameters();

                for (var j = 0; j < parameters.Length; j++)
                {
                    var parameter = parameters[j];

                    Console.WriteLine($"Parameter #{j}");
                    Console.WriteLine($"ParameterType: {parameter.ParameterType}.");
                    Console.WriteLine($"Name: {parameter.Name}.");
                    Console.WriteLine($"HasDefaultValue: {parameter.HasDefaultValue}.");

                    Console.WriteLine();
                }

                var args = parameters.Select(x => x.HasDefaultValue ? x.DefaultValue : Activator.CreateInstance(x.ParameterType)).ToArray();
                var instance = args.Length == 0 ? Activator.CreateInstance(type) : Activator.CreateInstance(type, args);

                Console.WriteLine(instance.ToString());
                Console.WriteLine();
            }
        }
    }
}
