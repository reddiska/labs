﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace LabB.Controllers
{
    public class Model
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }

    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        [Route("api/[controller]")]
        public ActionResult<IEnumerable<Model>> GetModelList()
        {
            return new List<Model>
            {
                new Model {Id = 0, Name = "Alena", Age = 21},
                new Model {Id = 1, Name = "Vitaliy", Age = 22}
            };
        }

        [HttpGet]
        [Route("api/[controller]/{id}")]
        public ActionResult<IEnumerable<Model>> GetModelById(int id)
        {
            var list = new List<Model>
            {
                new Model {Id = 0, Name = "Alena", Age = 21},
                new Model {Id = 1, Name = "Vitaliy", Age = 22}
            };

            var result = list.FirstOrDefault(x => x.Id == id);
            return result != null ? (ActionResult<IEnumerable<Model>>) Ok(result) : NotFound(); 
        }
    }
}