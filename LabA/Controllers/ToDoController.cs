﻿// ReSharper disable StringLiteralTypo

using System.Collections.Generic;
using System.Linq;
using LabA.Domain;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace LabA.Controllers
{
    [Route("api/todos")]
    [ApiController]
    public class ToDoController : ControllerBase
    {
        private readonly Repository _repository;

        public ToDoController(IHostingEnvironment hostingEnvironment)
        {
            _repository = new Repository(hostingEnvironment.ContentRootPath);
        }

        [HttpGet]
        public ActionResult<IEnumerable<ToDo>> GetAllToDos()
        {
            return _repository.GetAllToDos().ToList();
        }

        [HttpPost]
        public ActionResult<ToDo> CreateTodo([FromBody]ToDo toDo)
        {
            _repository.CreateToDo(toDo);
            return Ok(toDo);
        }

        [HttpPatch]
        public ActionResult<ToDo> UpdateTodo([FromBody]ToDo toDo)
        {
            _repository.UpdateToDo(toDo);
            return Ok(toDo);
        }

        [HttpDelete]
        public ActionResult<ToDo> DeleteToDo([FromBody]int id)
        {
            _repository.DeleteToDo(id);
            return NoContent();
        }
    }
}
