﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace LabA.Domain
{
    public class Repository
    {
        private readonly string _connectionString;

        public Repository(string contentRootPath)
        {
            _connectionString = $"Data Source=(localdb)\\MSSQLLocalDB;AttachDbFilename={contentRootPath}\\AppData\\ToDoDB.mdf;Integrated Security=True;";
        }

        public IEnumerable<ToDo> GetAllToDos()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                return db.Query<ToDo>("SELECT * FROM ToDos").ToList();
            }
        }

        public void CreateToDo(ToDo todo)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                const string sqlQuery = "INSERT INTO ToDos VALUES(@Id, @Label, @Important, @Done)";
                db.Execute(sqlQuery, todo);
            }
        }

        public void UpdateToDo(ToDo todo)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                const string sqlQuery = "UPDATE ToDos SET Label = @Label, Important = @Important, Done = @Done WHERE Id = @Id";
                db.Execute(sqlQuery, todo);
            }
        }

        public void DeleteToDo(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                const string sqlQuery = "DELETE FROM ToDos WHERE Id = @id";
                db.Execute(sqlQuery, new { id });
            }
        }
    }
}
