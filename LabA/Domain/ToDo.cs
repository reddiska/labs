﻿using System;

namespace LabA.Domain
{
    public class ToDo
    {
        public Guid Id { get; set; }
        public string Label { get; set; }
        public bool Important { get; set; }
        public bool Done { get; set; }
    }
}